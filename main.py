from helper import validate_input

#dictionary example
user_input=[]
while user_input != "exit":
    user_input = input("Enter your value  which are colon separated! \n")
    days_unit=user_input.split(":")
    dictionary_input={"days" : days_unit[0],"unit" : days_unit[1]}
    print(dictionary_input)
    validate_input(dictionary_input)

# # Example for list
# user_input=[]
# while user_input != "exit":
#     user_input = input("Enter your value user which are comma separated! \n")
#     for num_items in user_input.split(","): #split() is used to convert string to list, #split(",")  this used list with seperated
#         validate_input()


# user_input=""
# while user_input != "exit":
#     user_input = input("Enter your value user! \n")
#     validate_input()
